from flask import Flask, render_template
import requests
import json


app = Flask(__name__)
@app.route('/', methods=['GET', 'POST'])
def index():
    response = requests.get('http://api.fantasy.nfl.com/v1/players/stats?statType=seasonStats&season=2017&week=1&format=json')
    nfl_data = response.content
    json_dict = json.loads(nfl_data)
    arr = []
    for name in json_dict:
        arr = arr.append(name)

    return render_template('index.html', json_dict = json_dict, arr = arr)

if __name__ == '__main__':
	app.run(debug=True)