#!/bin/python3

import sys


n = int(input().strip())
arr = [int(arr_temp) for arr_temp in input().strip().split(' ')]
temp = []
i = len(arr)-1;
while i >= 0:
    arr[i] = str(arr[i])
    temp.append(arr[i])
    i = i - 1
print(' '.join(temp))