class Node:

    def __init__(self, data, next):
        self.data = data
        self.next = next

class LinkedList:

    def __init__(self):
        self.head = None

    def printList(self):
        temp = self.head
        if(temp is None):
            print(None)
        while(temp is not None):
            print(str(temp.data) + ' -> ', end="")
            temp = temp.next

    def addFront(self,data):
        if (self.head is None):
            self.head = Node(data,None)
        node = Node(data,None)
        node.next = self.head
        self.head = node

    def insertAfter(self, key, data):
        new_node = Node(data, None)
        new_node.next = key.next
        key.next = new_node


if __name__=='__main__':
    ll = LinkedList()
    ll.addFront(3)
    ll.addFront(2)
    ll.addFront(1)
    node1 = Node(3,None)
    ll.insertAfter(node1, 7)
    ll.printList()
